﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Skill
{
    public int Experience;
    public int Level;
    public Action OnLevelUp;
    public Skill(int level, Action OnLevUp)
    {
        Level = level;
        Experience = getXPForLevel(level);
        OnLevelUp = OnLevUp;
    }
    public int getXPForLevel(int level)
    {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= level; lvl++)
        {
            points += (int)Math.Floor((double)lvl + 300.0f * Math.Pow(2.0f, (double)lvl / 7.0f));
            if (lvl >= level)
                return output;
            output = (int)Math.Ceiling((double)points / 4);
        }
        return 0;
    }
    public static int getLevelForXP(int exp)
    {
        int points = 0;
        int output = 0;
        if (exp > 13034430)
            return 99;
        for (int lvl = 1; lvl <= 99; lvl++)
        {
            points += (int)Math.Floor((double)lvl + 300.0f * Math.Pow(2.0f, (double)lvl / 7.0f));
            output = (int)Math.Floor((double)points / 4);
            if (output >= exp)
            {
                return lvl;
            }
        }
        return 0;
    }
    /// <summary>
    /// Add skill exp to current skill
    /// </summary>
    /// <param name="skill">The current skill</param>
    /// <param name="amount">The amount of xp to add</param>
    /// <returns>Returns true if you level up</returns>
    public bool addSkillXP(int amount)
    {
        if (amount + Experience < 0 || Experience > 200000000)
        {
            if (Experience > 200000000)
            {
                Experience = 200000000;
            }
            return false;
        }
        int oldLevel = getLevelForXP(Experience);
        Experience += amount;
        if (oldLevel < getLevelForXP(Experience))
        {
            if (Level < getLevelForXP(Experience))
            {
                Level = getLevelForXP(Experience);
                if (OnLevelUp != null)
                    OnLevelUp.Invoke();
                return true;
            }
        }
        return false;
    }
}