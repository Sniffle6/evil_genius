﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class MinionManager : MonoBehaviour
{
    [SerializeField]
    List<Minion> c_Minions;
    [SerializeField]
    int c_MinionsUsed;
    [SerializeField]
    List<Minion> c_MinionsOnJob;
    [SerializeField]
    Text t_MinionsToAdd;
    [SerializeField]
    Text t_MinionCount;
    [SerializeField]
    Text t_AvailableMinions;
    [SerializeField]
    GameObject c_JobParent;

    public InputField[] c_JobInputFields;

    public static MinionManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    public List<Minion> getMinions()
    {
        return c_Minions;
    }
    // Use this for initialization
    void Start()
    {
        c_Minions.Add(new Minion("Jack Sparrow", 100));
        c_Minions[0].Level = Random.Range(1, 99);
        t_MinionCount.text = "Minion Count:\n" + c_Minions.Count.ToString();
        t_AvailableMinions.text = "Available Minions: " + (c_Minions.Count - c_MinionsUsed);
        c_JobInputFields = c_JobParent.GetComponentsInChildren<InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            for (int i = 0; i < c_Minions.Count; i++)
            {
                Debug.Log(c_Minions[i].GetJob.TimeInJob);
            }
        }
        for (var i = 0; i < c_Minions.Count; i++)
        {
            c_Minions[i].Update();
        }
    }
    public void RecruitMinionButton()
    {
        for (int i = 0; i < int.Parse(t_MinionsToAdd.text); i++)
        {
            c_Minions.Add(new Minion("Jack Sparrow", 100));
        }
        t_MinionCount.text = "Minion Count:\n" + c_Minions.Count.ToString();
        t_AvailableMinions.text = "Available Minions: " + (c_Minions.Count - c_MinionsUsed);
    }
    public void AddMinion(Minion minion)
    {
        c_Minions.Add(minion);
        t_MinionCount.text = "Minion Count:\n" + c_Minions.Count.ToString();
        t_AvailableMinions.text = "Available Minions: " + (c_Minions.Count - c_MinionsUsed);
    }
    public void doJob(int jobIndex)
    {
        var minionsToSend = int.Parse(c_JobInputFields[jobIndex].text);
        if (minionsToSend > c_Minions.Count - c_MinionsUsed)
            minionsToSend = c_Minions.Count - c_MinionsUsed;
        var counter = 0;
        for (var i = 0; i < c_Minions.Count; i++)
        {
            if (counter >= minionsToSend)
                break;
            if (c_Minions[i].GetJob.Active)
                continue;
            c_Minions[i].DrowDownValue(jobIndex);
            if (!c_Minions[i].DidSetJob())
                continue;
            counter++;
            c_MinionsUsed++;


        }
        AvailableMinions = "Available Minions: " + (c_Minions.Count - c_MinionsUsed);
        //}
        //    print(c_JobInputFields[jobIndex].text);
        //    if (int.Parse(c_JobInputFields[jobIndex].text) > c_Minions.Count - c_MinionsUsed)
        //        return;
        //    c_MinionsUsed += int.Parse(c_JobInputFields[jobIndex].text);
        //    t_AvailableMinions.text = "Available Minions: " + (c_Minions.Count - c_MinionsUsed);
        //    GetComponent<NumberTwoLevel>().AddXp(int.Parse(c_JobInputFields[jobIndex].text) * (jobIndex + 1));
    }
    public string AvailableMinions
    {
        get { return t_AvailableMinions.text; }
        set { t_AvailableMinions.text = value; }
    }
    public int MinionsUsed
    {
        get { return c_MinionsUsed; }
        set { c_MinionsUsed = value; }
    }
    public int MinionCount
    {
        get { return c_Minions.Count; }
    }
}
