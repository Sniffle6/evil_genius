﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PopulatePanel : MonoBehaviour
{
    MinionManager mm;
    List<Minion> p_minions;
    [SerializeField]
    GameObject MinionPrefab;
    [SerializeField]
    RectTransform parent;
    int minionInPanel = 0;
    float minionPanelHeight;
    //public static List<GameObject> exisitngMinions;
    JobAsset j;
    // Use this for initialization
    void Start()
    {
        // exisitngMinions = new List<GameObject>();
        j = Resources.Load("Jobs/Steal From Kids") as JobAsset;
        mm = MinionManager.instance;
        p_minions = mm.getMinions();
        MinionPrefab = Resources.Load("Minion") as GameObject;
        minionPanelHeight = MinionPrefab.GetComponent<RectTransform>().sizeDelta.y;
        PushToPanel();
    }

    // Update is called once per frame
    static int cap;
    void Update()
    {
        PushToPanel();
        for (var i = 0; i < p_minions.Count; i++)
        {
            updateText(p_minions[i]);
        }
    }
    void PushToPanel()
    {
        if (minionInPanel != p_minions.Count)
        {
            for (var i = minionInPanel; i < p_minions.Count; i++)
            {
                var clone = MinionPrefab;
                var panel = Instantiate(clone, parent) as GameObject;
                panel.transform.localPosition = new Vector3(0, 0 - i * minionPanelHeight, 0);
                p_minions[i].MinionPanel = panel;
                p_minions[i].MinionPanel.GetComponentInChildren<Dropdown>().onValueChanged.AddListener(p_minions[i].DrowDownValue);
                p_minions[i].MinionPanel.GetComponentInChildren<Dropdown>().value = p_minions[i].Value;
                p_minions[i].MinionPanel.GetComponentInChildren<Button>().onClick.AddListener(p_minions[i].SetJob);
                p_minions[i].MinionPanel.transform.Find("time").GetComponent<Text>().text = "Not in job";
                p_minions[i].MinionPanel.transform.Find("name").GetComponent<Text>().text = p_minions[i].GetName;
                p_minions[i].MinionPanel.transform.Find("level").GetComponent<Text>().text = p_minions[i].GetSkill.Level.ToString();
                p_minions[i].MinionPanel.transform.Find("exp").GetComponent<Text>().text = p_minions[i].GetSkill.Experience.ToString("n0") + "/" + p_minions[i].GetSkill.getXPForLevel(p_minions[i].GetSkill.Level + 1).ToString("n0");
            }
            minionInPanel = p_minions.Count;
        }
    }
    void updateText(Minion min)
    {
        if (min.GetJob.Active)
            min.MinionPanel.transform.Find("time").GetComponent<Text>().text = min.GetJob.TimeLeft.ToString();
        else
            min.MinionPanel.transform.Find("time").GetComponent<Text>().text = "Not in job";
    }
}
