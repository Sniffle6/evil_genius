﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Unamed Job", menuName = "Job", order = 1)]
public class JobAsset : ScriptableObject
{

    public string Name;
    public int Experience;
    public int LevelRequirment;
    public Vector2 MinMaxJobLength;
    public GameObject[] Rewards;
}
