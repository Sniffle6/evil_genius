﻿using UnityEngine;
using System.Collections.Generic;
using SCG.General;


public static class RandomNameGenerator
{
    static List<string> firstNameSyllables = new List<string>(new string[] { "mel", "fay", "che", "bra", "car", "rash", "izen", "co", "an", "dil", "lin", "bry", "an", "lin", "jin", "es", "jay" });
    static List<string> lastNameSyllables = new List<string>(new string[] { "malo", "zak", "abo", "wonk", "web", "ber", "jor", "dan", "poz", "zon" });

    public static List<string> FirstNames = new List<string>(new string[] { 
"Arthur",
"Arnie",
"Aaron",
"Abraham",
"Anthony",
"Alexander",
"Andrew",
"Aiden",
"Austin",
"Abram",
"Ace",
"Allen",
"Amir",
"Ali",
"Alan",
"Aidan",
"Anderson",
"Andres",
"Alec",
"Andy",
"Alex",
"Bryan",
"Bob",
"Bobby",
"Barry",
"Bill",
"Berry",
"Benny",
"Borack",
"Bret",
"Charley",
"Cody",
"Corbin",
"Codie",
"Caden",
"Christopher",
"Chris",
"Dean",
"Donny",
"Donald",
"Denis",
"Demetre",
"Dante",
"Danny",
"Danielle",
"Dennis",
"Eric",
"Ernie",
"Erin",
"Earl",
"Felix",
"Fran",
"Frances",
"Gordon",
"Henri",
"Henry",
"Hanson",
"Harvey",
"Ivan",
"Isaac",
"Jerry",
"Jackson",
"Jeanne",
"Kyle",
"Karl",
"Lee",
"Liam",
"Lucas",
"Luke",
"Mason",
"Marco",
"Michael",
"Nicholas",
"Nate",
"Noah",
"Oscar",
"Peter",
"Philippe",
"Richard",
"Sebastien",
"Sam",
"Stan",
"Tedy",
"Tomas",
"Tony",
"Wilfred",
"Walter",
"William"});



    static List<string> lastNames = new List<string>(new string[] {  "Abbott",
  "Acevedo",
  "Acosta",
  "Adams",
  "Adkins",
  "Aguilar",
  "Aguirre",
  "Albert",
  "Alexander",
  "Alford",
  "Allen",
  "Allison",
  "Alston",
  "Alvarado",
  "Alvarez",
  "Anderson",
  "Andrews",
  "Anthony",
  "Armstrong",
  "Arnold",
  "Ashley",
  "Atkins",
  "Atkinson",
  "Austin",
  "Avery",
  "Avila",
  "Ayala",
  "Ayers",
  "Bailey",
  "Baird",
  "Baker",
  "Baldwin",
  "Ball",
  "Ballard",
  "Banks",
  "Barber",
  "Barker",
  "Barlow",
  "Barnes",
  "Barnett",
  "Barr",
  "Barrera",
  "Barrett",
  "Barron",
  "Barry",
  "Bartlett",
  "Barton",
  "Bates",
  "Battle",
  "Bauer",
  "Baxter",
  "Beach",
  "Bean",
  "Beard",
  "Beasley",
  "Beck",
  "Becker",
  "Bell",
  "Bender",
  "Benjamin",
  "Bennett",
  "Benson",
  "Bentley",
  "Benton",
  "Berg",
  "Berger",
  "Bernard",
  "Berry",
  "Best",
  "Bird",
  "Bishop",
  "Black",
  "Taylor",
  "Jordan",
    "Weber"});

    // Use this for initialization

    public static string CreateNewName()
    {
        //Creates a first name with 2-3 syllables
        var firstName = "";
        var numberOfSyllablesInFirstName = Random.Range(2, 4);
        for (int i = 0; i < numberOfSyllablesInFirstName; i++)
        {
            firstName += firstNameSyllables[Random.Range(0, firstNameSyllables.Count)];
        }
        var firstNameLetter = "";
        firstNameLetter = firstName.Substring(0, 1);
        firstName = firstName.Remove(0, 1);
        firstNameLetter = firstNameLetter.ToUpper();
        firstName = firstNameLetter + firstName;

        //Creates a last name with 1-2 syllables
        var lastName = "";
        var numberOfSyllablesInLastName = Random.Range(1, 3);
        for (var j = 0; j < numberOfSyllablesInLastName; j++)
        {
            lastName += lastNameSyllables[Random.Range(0, lastNameSyllables.Count)];
        }
        var lastNameLetter = "";
        lastNameLetter = lastName.Substring(0, 1);
        lastName = lastName.Remove(0, 1);
        lastNameLetter = lastNameLetter.ToUpper();
        lastName = lastNameLetter + lastName;
        var curName = firstName + " " + lastName;
        //assembles the newly-created name
        Debug.Log("First: " + firstName);
        Debug.Log("Last: " + lastName);
        return curName;
    }
    static MarkovNameGenerator fNames = new MarkovNameGenerator(FirstNames, 3, 2);
    static MarkovNameGenerator lNames = new MarkovNameGenerator(lastNames, 2, 3);
    public static string MarkovName()
    {

        return fNames.NextName + " " + lNames.NextName;
    }
}