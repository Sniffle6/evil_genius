﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[Serializable]
public class Minion
{
    [SerializeField]
    string m_Name;
    [SerializeField]
    Health m_health;
    [SerializeField]
    Skill m_Level;
    [SerializeField]
    Job m_Job;

    [SerializeField]
    int m_Price;

    [SerializeField]
    GameObject m_MinionPanel;

    Text exp;
    Text level;
    float randomPrice = UnityEngine.Random.Range(180, 280);
    [SerializeField]
    int d_Value;
    public Minion(string name)
    {
        m_Name = name;
        m_health = new Health(100);
        m_Level = new Skill(1, OnLevelUp);
        m_Job = new Job();
    }
    public Minion(string name, float MaxHp)
    {
        m_Name = name;
        m_health = new Health(MaxHp);
        m_Level = new Skill(1, OnLevelUp);
        m_Job = new Job();
    }
    public Minion(string name, float MaxHp, int lev)
    {
        m_Name = name;
        m_health = new Health(MaxHp);
        m_Level = new Skill(lev, OnLevelUp);
        m_Job = new Job();
    }
    public string GetName
    {
        get { return m_Name; }
    }
    public void AddMinion()
    {
        if (Money.instance.money < Price)
        {
            Chat.AddChatEntry("Not enough money!");
            return;
        }
        MinionManager.instance.AddMinion(this);
        HireMinions.instance.HirableMinions.Remove(this);
        MinionPanel.SetActive(false);
        Money.instance.money -= Price;
        Chat.AddChatEntry(GetName + " was hired!");
    }
    public Health GetHp
    {
        get { return m_health; }
    }
    public Skill GetSkill
    {
        get { return m_Level; }
    }
    public int Level
    {
        get { return m_Level.Level; }
        set { m_Level.Level = value; m_Level.Experience = m_Level.getXPForLevel(value); }

    }
    public int Price
    {
        get
        {
            var p = 0;
            p = Level * (int)randomPrice;
            m_Price = p;
            return m_Price;
        }
    }
    public GameObject MinionPanel
    {
        get { return m_MinionPanel; }
        set
        {
            m_MinionPanel = value;
            if (m_MinionPanel.transform.Find("exp"))
                exp = m_MinionPanel.transform.Find("exp").GetComponent<Text>();
            if (m_MinionPanel.transform.Find("level"))
                level = m_MinionPanel.transform.Find("level").GetComponent<Text>();
        }
    }
    public Job GetJob
    {
        get { return m_Job; }
    }
    public void DrowDownValue(int arg0)
    {
        d_Value = arg0;
    }
    public int Value
    {
        get { return d_Value; }
    }
    public void OnLevelUp()
    {
        Chat.AddChatEntry(m_Name + " has level'd up!");
    }
    public bool DidSetJob()
    {
        if (m_Job.GetJob(Value).LevelRequirment > GetSkill.Level)
        {
            Chat.AddChatEntry(GetName + "'s level is not high enough!");
            Chat.AddChatEntry("Level " + m_Job.GetJob(Value).LevelRequirment + " is required!");
            return false;
        }
        if (m_Job.Active)
            return false;
        m_Job.Type = m_Job.GetJob(Value);
        m_Job.Active = true;
        return true;
    }
    public void SetJob()
    {
        if (m_Job.GetJob(Value).LevelRequirment > GetSkill.Level)
        {
            Chat.AddChatEntry(GetName + "'s level is not high enough!");
            Chat.AddChatEntry("Level " + m_Job.GetJob(Value).LevelRequirment + " is required!");
            return;
        }
        if (m_Job.Active)
        {
            return;
        }
        Chat.AddChatEntry(GetName + " left for Job!");
        MinionManager.instance.MinionsUsed += 1;
        MinionManager.instance.AvailableMinions = "Available Minions: " + (MinionManager.instance.MinionCount - MinionManager.instance.MinionsUsed);
        m_Job.Type = m_Job.GetJob(Value);
        m_Job.Active = true;
    }
    public void JobComplete()
    {
        m_Level.addSkillXP(m_Job.Type.Experience);
        NumberTwoLevel.instance.AddXp(m_Job.Type.Experience / 3);
        Chat.AddChatEntry(GetName + " has finished job '" + GetJob.Type.name + "'!");
        Money.instance.money += m_Job.Type.Experience;
        if (MinionPanel)
            MinionPanel.GetComponentInChildren<Dropdown>().value = Value;
        m_Job.Type = null;
        m_Job.Active = false;
        MinionManager.instance.MinionsUsed -= 1;
        MinionManager.instance.AvailableMinions = "Available Minions: " + (MinionManager.instance.MinionCount - MinionManager.instance.MinionsUsed);
    }
    public void Update()
    {
        if (m_Job.Active)
        {
            if (m_Job.TimeLeft <= 0)
            {
                JobComplete();
                if (MinionPanel)
                {
                    exp.text = GetSkill.Experience.ToString("n0") + "/" + GetSkill.getXPForLevel(GetSkill.Level + 1).ToString("n0");
                    level.text = GetSkill.Level.ToString();
                }
            }
        }
    }
}
