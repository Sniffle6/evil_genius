﻿using UnityEngine;
using System;
[Serializable]
public class Health  {
    [SerializeField]
    public float MaxHp;
    [SerializeField]
    public float CurrentHp;
    public Health(float maxHp)
    {
        MaxHp = maxHp;
        CurrentHp = MaxHp;
    }
}
