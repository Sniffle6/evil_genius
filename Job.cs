﻿using UnityEngine;
using System.Collections;
using System;



[Serializable]
public class Job
{
    [SerializeField]
    JobAsset m_job;

    [SerializeField]
    bool m_DoingJob;

    [SerializeField]
    float m_JobStartTime;

    [SerializeField]
    float m_JobLength;

    [SerializeField]
    float m_JobEndTime;

    public Job()
    {
        m_DoingJob = false;
    }
    public Job(JobAsset job, bool doJob)
    {
        m_job = job;
        m_DoingJob = doJob;
        if (m_DoingJob)
        {
            m_JobStartTime = Time.time;
            m_JobLength = UnityEngine.Random.Range(m_job.MinMaxJobLength.x, m_job.MinMaxJobLength.y);
        }
    }
    public bool Active
    {
        get { return m_DoingJob; }
        set
        {
            m_DoingJob = value;
            if (value)
            {
                m_JobStartTime = Time.time;
                m_JobLength = UnityEngine.Random.Range(m_job.MinMaxJobLength.x, m_job.MinMaxJobLength.y);
                m_JobEndTime = m_JobStartTime + m_JobLength;
            }
        }
    }
    public JobAsset Type
    {
        get { return m_job; }
        set { m_job = value; }
    }
    public float TimeInJob
    {
        get { return Time.time - m_JobStartTime; }
    }
    public float TimeLeft
    {
        get { return (int)(m_JobLength - TimeInJob); }
    }
    public JobAsset GetJob(int value)
    {
        switch (value)
        {
            case 0:
                return Resources.Load("Jobs/Steal From Kids") as JobAsset;
            case 1:
                return Resources.Load("Jobs/Rob Old Granny") as JobAsset;
            case 2:
                return Resources.Load("Jobs/Pickpocket") as JobAsset;
            case 3:
                return Resources.Load("Jobs/Rob Small Business") as JobAsset;
            default:
                return null;
        }
    }
}
