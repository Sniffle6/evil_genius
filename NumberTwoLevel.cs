﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NumberTwoLevel : MonoBehaviour
{

    public Text t_Experience;
    public Text t_Level;
    [SerializeField]
    public Skill c_Level;

    public static NumberTwoLevel instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        c_Level = new Skill(1, LevelUp);
        t_Level.text = "Level: " + c_Level.Level;
        t_Experience.text = "Exp: " + c_Level.Experience.ToString("n0") + "/" + c_Level.getXPForLevel(c_Level.Level + 1).ToString("n0") + "\nRemaining XP: " + (c_Level.getXPForLevel(c_Level.Level + 1) - c_Level.Experience).ToString("n0");
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddXp(100);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            PrintRequiredXP(99);
        }

    }
    public void PrintRequiredXP(int levelToStopAt)
    {
        for (var i = 0; i<=levelToStopAt;i++)
        {
            print("Exp to reach level: "+ i + " is: " + c_Level.getXPForLevel(i));
        }
    }
    public void AddXp(int value)
    {
        c_Level.addSkillXP(value);
        t_Level.text = "Level: " + c_Level.Level;
        t_Experience.text = "Exp: " + c_Level.Experience.ToString("n0") + "/" + c_Level.getXPForLevel(c_Level.Level + 1).ToString("n0") + "\nRemaining XP: " + (c_Level.getXPForLevel(c_Level.Level + 1) - c_Level.Experience).ToString("n0");

    }
    void LevelUp()
    {
        Chat.AddChatEntry("Congratulations! Your level is now " + c_Level.Level);
    }
}
