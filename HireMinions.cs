﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using SCG.General;

public class HireMinions : MonoBehaviour
{
    GameObject p_MinionToHire;
    [SerializeField]
    List<Minion> l_MinionsToHire;
    [SerializeField]
    RectTransform parent;

    [SerializeField]
    float LastRepopulated;
    float y;
    public static HireMinions instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        p_MinionToHire = Resources.Load("Hire Minion") as GameObject;
        for (int i = 0; i < 10; i++)
        {
            l_MinionsToHire.Add(new Minion(RandomNameGenerator.MarkovName(), 100, Random.Range((int)1, (int)11)));
        }
        y = p_MinionToHire.GetComponent<RectTransform>().sizeDelta.y;
        for (int i = 0; i < l_MinionsToHire.Count; i++)
        {
            var clone = p_MinionToHire;
            var panel = Instantiate(clone, parent) as GameObject;
            panel.transform.localPosition = new Vector3(0, 0 - i * y, 0);
            l_MinionsToHire[i].MinionPanel = panel;
            l_MinionsToHire[i].MinionPanel.GetComponentInChildren<Button>().onClick.AddListener(l_MinionsToHire[i].AddMinion);
            l_MinionsToHire[i].MinionPanel.GetComponentInChildren<Button>().onClick.AddListener(RemoveMinionToHire);
            l_MinionsToHire[i].MinionPanel.transform.Find("name").GetComponent<Text>().text = l_MinionsToHire[i].GetName;
            l_MinionsToHire[i].MinionPanel.transform.Find("level").GetComponent<Text>().text = l_MinionsToHire[i].GetSkill.Level.ToString();
            l_MinionsToHire[i].MinionPanel.transform.Find("exp").GetComponent<Text>().text = l_MinionsToHire[i].GetSkill.Experience.ToString("n0") + "/" + l_MinionsToHire[i].GetSkill.getXPForLevel(l_MinionsToHire[i].GetSkill.Level + 1).ToString("n0");
            l_MinionsToHire[i].MinionPanel.transform.Find("cost").GetComponent<Text>().text = "$" + l_MinionsToHire[i].Price.ToString("n0");

        }
        LastRepopulated = Time.time;
    }
    void OnEnable()
    {
        if (Time.time - LastRepopulated < 30)
        {
            Debug.Log(Time.time - LastRepopulated);
            return;
        }
        l_MinionsToHire.Clear();
        for (int i = 0; i < 10; i++)
        {
            l_MinionsToHire.Add(new Minion(RandomNameGenerator.MarkovName(), 100, Random.Range((int)1, (int)11)));
        }
        RemoveMinionToHire();
        LastRepopulated = Time.time;
    }
    public List<Minion> HirableMinions
    {
        get { return l_MinionsToHire; }
    }

    void RemoveMinionToHire()
    {
        for (var i = 0; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
        for (int i = 0; i < l_MinionsToHire.Count; i++)
        {
            var clone = p_MinionToHire;
            var panel = Instantiate(clone, parent) as GameObject;
            panel.transform.localPosition = new Vector3(0, 0 - i * y, 0);
            l_MinionsToHire[i].MinionPanel = panel;
            l_MinionsToHire[i].MinionPanel.GetComponentInChildren<Button>().onClick.AddListener(l_MinionsToHire[i].AddMinion);
            l_MinionsToHire[i].MinionPanel.GetComponentInChildren<Button>().onClick.AddListener(RemoveMinionToHire);
            l_MinionsToHire[i].MinionPanel.transform.Find("name").GetComponent<Text>().text = l_MinionsToHire[i].GetName;
            l_MinionsToHire[i].MinionPanel.transform.Find("level").GetComponent<Text>().text = l_MinionsToHire[i].GetSkill.Level.ToString();
            l_MinionsToHire[i].MinionPanel.transform.Find("exp").GetComponent<Text>().text = l_MinionsToHire[i].GetSkill.Experience.ToString("n0") + "/" + l_MinionsToHire[i].GetSkill.getXPForLevel(l_MinionsToHire[i].GetSkill.Level + 1).ToString("n0");
            l_MinionsToHire[i].MinionPanel.transform.Find("cost").GetComponent<Text>().text = "$" + l_MinionsToHire[i].Price.ToString("n0");

        }
    }
    // Update is called once per frame
    void Update()
    {
    }
}
