﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Chat : MonoBehaviour {

    [SerializeField]
    RectTransform parent;
    static List<GameObject> chatEntries = new List<GameObject>();

    public static Chat instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {

            AddChatEntry("Test"+chatEntries.Count);
        }
    }
    public static void AddChatEntry(string message)
    {
        var obj = NewText();
        var rect = obj.GetComponent<RectTransform>();
        var text = obj.GetComponent<Text>();
        rect.sizeDelta = new Vector2(320, 20);
        rect.transform.localPosition = new Vector3(0, 0, 0);
        text.color = Color.black;
        text.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        
        text.text = message;
        for (var i = 0; i < chatEntries.Count; i++)
        {
            chatEntries[i].transform.position += new Vector3(0,20,0);
        }
        chatEntries.Add(obj);
    }
    public static GameObject NewText()
    {
        GameObject newText = new GameObject("TextLine");
        newText.AddComponent<Text>();
        newText.transform.SetParent(Chat.instance.parent);
        return newText;
        
    } 
}
