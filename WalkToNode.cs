﻿using UnityEngine;
using System.Collections;

public class WalkToNode : MonoBehaviour {
    #region Variables
    Transform m_Transform;
    [SerializeField]
    PlayerState m_CurrentState;
    [SerializeField]
    PlayerLocation m_CurrentLocation;
    Transform m_Target;
    GameObject m_Clicked;
    StateMachine sm_State;
    #endregion
    #region Start
    // Use this for initialization
    void Start () {
        m_Transform = gameObject.transform;
        m_CurrentState = PlayerState.Idle;
        m_CurrentLocation = PlayerLocation.Roaming;
    }
    #endregion
    // Update is called once per frame
    void Update ()
    {
        #region Mouse Click
        if (Input.GetButtonDown("Fire1"))
        {
            var mainCamera = FindCamera();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin, mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
            {
                if (hit.collider.transform.parent.tag == "Prop")
                {
                    m_Clicked = hit.collider.transform.parent.gameObject;
                    StateChange(PlayerState.Moving);
                }
            }
        }
        #endregion
        #region State Machine Update
        if (sm_State == StateMachine.Start)
        {
            switch (m_CurrentState)
            {
                case PlayerState.Idle:

                    break;
                case PlayerState.Moving:
                    m_Transform.position = Vector3.Lerp(m_Transform.position, m_Target.position, Time.deltaTime*2f);
                    if (Vector3.Distance(m_Transform.position,m_Target.position) < 0.01f)
                    {
                        StateChange(PlayerState.Idle);
                        switch (m_Target.parent.name)
                        {
                            case "Desk":
                                ChangeLocation(PlayerLocation.Desk);
                                break;
                            case "Computer":
                                ChangeLocation(PlayerLocation.Computer);
                                break;
                            case "Power Core":
                                ChangeLocation(PlayerLocation.Power_Core);
                                break;
                            case "Minion":
                                ChangeLocation(PlayerLocation.Minion);
                                break;
                            default:
                                ChangeLocation(PlayerLocation.Undefined);
                                break;
                        }
                    }
                    break;
            }
        }
        #endregion
    }
    #region Location States
    void ChangeLocation(PlayerLocation location)
    {
        LocationExit();
        m_CurrentLocation = location;
        LocationPreStart();
        LocationStart();
    }
    void LocationExit()
    {

    }
    void LocationPreStart()
    {

    }
    void LocationStart()
    {

    }
    #endregion
    #region State_Machine
    void StateChange(PlayerState nextState)
    {
        StateExit();
        m_CurrentState = nextState;
        StatePreStart();
        StateStart();
    }
    void StatePreStart()
    {
        sm_State = StateMachine.PreStart;
        m_Target = m_Clicked.transform.FindChild("Stand Point");
    }
    void StateStart()
    {
        sm_State = StateMachine.Start;
    }
    void StateExit()
    {
        sm_State = StateMachine.Exit;
        ChangeLocation(PlayerLocation.Roaming);
    }
    #endregion

    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
}
#region Enumerators
public enum PlayerState
{
    Idle,
    Moving
}
public enum PlayerLocation
{
    Roaming,
    Desk,
    Computer,
    Power_Core,
    Minion,
    Undefined
}
public enum StateMachine
{
    PreStart,
    Start,
    Exit
}
#endregion